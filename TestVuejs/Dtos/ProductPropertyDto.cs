﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Dtos
{
    public class ProductPropertyDto
    {
       public Guid ProductId { get; set; }
       public List<ProductFieldDto> ProductFields { get; set; }
    }

    public class ProductFieldDto
    {
        public Guid CategoryFieldId { get; set; }
        public string Value { get; set; }
    }
}

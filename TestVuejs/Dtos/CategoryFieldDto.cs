﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Dtos
{
    public class CategoryFieldDto
    {
        public Guid CategoryId { get; set; }
        public List<string> Name { get; set; }
    }
}

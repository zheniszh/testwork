﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Dtos
{
    public class CategoryDto
    {
        public string Name { get; set; }
    }
}

﻿import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import AllProducts from './Components/Product/AllProducts.vue'
import AddProduct from './Components/Product/AddProduct.vue'
import axios from '../node_modules/axios'
import ElementUI from '../node_modules/element-ui';
import '../node_modules/element-ui/lib/theme-chalk/index.css';
import locale from '../node_modules/element-ui/lib/locale/lang/ru-RU'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(ElementUI, {locale });
Vue.prototype.$http = axios;
Vue.prototype.$siteurl = "http://localhost:52697/api/"

const routes = [
    {
        path: '/',
        component: App
    },
    {
        path: '/products',
        component: AllProducts
    },
    {
        path: '/product/add',
        component: AddProduct
    }
]

const router = new VueRouter({
    routes,
    mode: 'history'
})

new Vue({
    el: '#app',
    template: "<div><router-view></router-view></div>",
    router
})
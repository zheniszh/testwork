﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TestVuejs.Models
{
    public class MarketPlaceContext : DbContext
    {
        public MarketPlaceContext (DbContextOptions<MarketPlaceContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<CategoryField>()
            //    .HasOne<Category>(s => s.Category)
            //    .WithMany(g => g.CategoryFields)
            //    .HasForeignKey(s => s.CategoryId);
          
            //modelBuilder.Entity<ProductProperty>()
            //    .HasOne<Product>(s => s.Product)
            //    .WithMany(g => g.ProductProperties)
            //    .HasForeignKey(s => s.ProductId);


        }

        public DbSet<Category> Category { get; set; }
        public DbSet<CategoryField> CategoryField { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductProperty> ProductProperty { get; set; }

    }
}

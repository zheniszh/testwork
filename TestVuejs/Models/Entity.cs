﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Models
{
    public class Entity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreaDateTime { get; set; }
        public bool Hide { get; set; }
    }
}

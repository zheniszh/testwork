﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Models
{
    public class ProductProperty
    {
        public Guid Id { get; set; }
        public Guid CategoryFieldId { get; set; }
        public Guid ProductId { get; set; }
        public string Value { get; set; }
        public bool Hide { get; set; }
        
    }
}

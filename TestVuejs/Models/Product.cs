﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Models
{
    public class Product : Entity
    {
        public Guid CategoryId { get; set; }

        public ICollection<ProductProperty> ProductProperties { get; set; }
        public int Price { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Models
{
    public class CategoryField : Entity
    {
        public Guid CategoryId { get; set; }
        public ICollection<ProductProperty> ProductProperties { get; set; }
    }
}

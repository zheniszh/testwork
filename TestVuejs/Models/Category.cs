﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestVuejs.Models
{
    public class Category : Entity
    {

        public ICollection<CategoryField> CategoryFields { get; set; }
        
    }
}

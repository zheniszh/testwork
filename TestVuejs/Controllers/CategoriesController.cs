﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestVuejs.Dtos;
using TestVuejs.Models;

namespace TestVuejs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly MarketPlaceContext _context;

        public CategoriesController(MarketPlaceContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        //Получение все категории
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategory()
        {
            var categories = await _context.Category.Where(s=>s.Hide!=true).ToListAsync();
            await _context.CategoryField.Where(cf => categories.Select(c => c.Id).Contains(cf.CategoryId))
                .ToListAsync();
            return categories;
        }

        // GET: api/Categories/5 
        // получение полей катеогрии вместе со значениями полей продуктов
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CategoryField>>> GetCategoryFieldswithProductProperties(Guid id)
        {
            var categoryFields = await _context.CategoryField.Where(s=>s.CategoryId==id).ToListAsync();


            if (categoryFields == null)
            {
                return NotFound();
            }
            await _context.ProductProperty.Where(pp => categoryFields.Select(cf => cf.Id).Contains(pp.CategoryFieldId) && pp.Hide!=true)
                .ToListAsync();

            return categoryFields;
        }



        // PUT: api/Categories/5
        //Удаление категории
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(Guid id)
        {

            var category = await _context.Category.FindAsync(id);
            category.Hide = true;
            _context.Category.Update(category);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Categories
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory([FromBody]CategoryDto dto)
        {
            var category = new Category
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                CreaDateTime = DateTime.Now,
                Hide = false
            };
            _context.Category.Add(category);
            await _context.SaveChangesAsync();

            return Ok(new { id = category.Id });
        }

        [HttpPost("fields")]
        public async Task<IActionResult> PostFields([FromBody] CategoryFieldDto dto)
        {

            foreach (var field in dto.Name)
            {
                var CategoryField = new CategoryField
                {
                    Id = Guid.NewGuid(),
                    Name = field,
                    CategoryId = dto.CategoryId,
                    CreaDateTime = DateTime.Now,
                    Hide = false
                };
                _context.CategoryField.Add(CategoryField);
            }

            await _context.SaveChangesAsync();


            return Ok();

        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Category>> DeleteCategory(Guid id)
        {
            var category = await _context.Category.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            _context.Category.Remove(category);
            await _context.SaveChangesAsync();

            return category;
        }

        private bool CategoryExists(Guid id)
        {
            return _context.Category.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestVuejs.Dtos;
using TestVuejs.Models;

namespace TestVuejs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly MarketPlaceContext _context;

        public ProductsController(MarketPlaceContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProduct()
        {
            return await _context.Product.Where(s=>s.Hide!=true).ToListAsync();
        }

        [HttpGet("filterbycategory/{categoryId}")]
        public async Task<ActionResult<IEnumerable<Product>>> GetFilteredByCategory(Guid categoryId)
        {
            var products = await _context.Product.Where(s => s.CategoryId == categoryId && s.Hide!=true).ToListAsync();
            await _context.ProductProperty.Where(pp => products.Select(cf => cf.Id).Contains(pp.ProductId))
                .ToListAsync();
            return products;
        }

    
        // GET: api/Products/5
        //Получение продуктов по определенной категории
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            

            if (product == null)
            {
                return NotFound();
            }
            //await _context.ProductProperty.Where(s => s.ProductId == id).ToListAsync();

            return product;
        }

        // PUT: api/Products/5
        // Удаление провдукта и его свойтсв
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            product.Hide = true;
            _context.Product.Update(product);
            var productsproperies = await _context.ProductProperty.Where(s => s.ProductId == id).ToListAsync();
            foreach (var productProperty in productsproperies)
            {
                productProperty.Hide = true;
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        // получение все продуктов
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct([FromBody]ProductDto dto)
        {
            var product = new Product
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                CategoryId = dto.CategoryId,
                Price = dto.Price,
                CreaDateTime = DateTime.Now,
                Hide = false
            };
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return product;
        }

        [HttpPost("property")]
        public async Task<IActionResult> AddProductProperty([FromBody] ProductPropertyDto dto)
        {
      
            foreach (var properyDto in dto.ProductFields)
            {
                var prodProperty = new ProductProperty
                {
                    Id = Guid.NewGuid(),
                    ProductId = dto.ProductId,
                    CategoryFieldId = properyDto.CategoryFieldId,
                    Value = properyDto.Value
                };
                _context.ProductProperty.Add(prodProperty);
            }

            await _context.SaveChangesAsync();
            return Ok();
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(Guid id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(Guid id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
